import React, { PropTypes } from 'react';

require('../../../styles/sudoku.scss');

class SudokuTable extends React.Component {
  render() {
    const { data, size } = this.props;

    const renderTable = () => {
      const dom = [];
      for (let i = 0; i < size; i++) {
        dom.push(renderTr(i));
      }
      return dom;
    };

    const renderTr = (idx) => {
      const tr = [];
      for (let i = 0; i < size; i++) {
        tr.push(
          <td key={`td_${idx}_${i}`}>
            {data[idx][i]}
          </td>
        );
      }
      return (
        <tr>
          {tr}
        </tr>
      );
    };

    return (
      <div className={`${this.props.className}`}>
        <table className='sudoku-table'>
          <tbody>
            {renderTable()}
          </tbody>
        </table>
      </div>
    );
  }
}

class Chatroom extends React.Component {
  render() {
    return (
      <ul className={`list-group ${this.props.className}`}>
        <li className="list-group-item">현재 접속자 5명</li>
        <li className="list-group-item">Dapibus ac facilisis in</li>
      </ul>
    )
  }
}

export default class Main extends React.Component {
  state = {
    data : 
      [
        [1,2,3],
        [1,2,3],
        [1,2,3],
      ],
  }

  componentDidMount = () => {
    // this.init();
  }

  init = () => {
  }

  handleChange = (e) => {
    this.setState({ [`${e.target.name}`]: e.target.value }, () => this.init());
  }

  render() {
    const { data } = this.state;
    return (
      <div className="algo-container row">
        <SudokuTable
          className="col-lg-7"
          data={data}
          size={data.length}
        />
        <Chatroom 
          className="col-lg-5"
        />
      </div>
    );
  }
}

Main.propTypes = {
};
