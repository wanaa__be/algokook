import React, { PropTypes } from 'react';
import highlight from 'highlight.js';

require('highlight.js/styles/vs.css');

export default class SourceCode extends React.Component {
  componentDidMount() {
    highlight.initHighlightingOnLoad();
  }
  componentDidUpdate() {
    setTimeout(highlight.highlightBlock(this.refs.sourceCode), 0);
  }
  render() {
    return (
      <pre>
        <code className="c++" ref="sourceCode">
          {this.props.children}
        </code>
      </pre>
    );
  }
}

SourceCode.propTypes = {
  children: PropTypes.string.isRequired,
};
