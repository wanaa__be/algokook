import React, { PropTypes } from 'react';
import vis from 'vis';

export default class TreeView extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  state = {
    data: [],
  };

  shouldComponentUpdate(nextProps) {
    return nextProps.data.length !== this.props.data.length;
  }

  handleClick(param) {
    this.props.handleClick(param.nodes[0] + 1);
  }

  render() {
    const { data, sort, template, treeDirection } = this.props;
    const nodes = new vis.DataSet();
    const edges = new vis.DataSet();
    const container = document.getElementById('mynetwork');
    const option = {
      layout: {
        hierarchical: {
          direction: treeDirection,
          sortMethod: 'directed',
        },
      },
      interaction: {
        // zoomView: false,
        selectConnectedEdges: false,
      },
    };
    

    const imgRenderer = (label, color) => {
      const html = '<svg xmlns="http://www.w3.org/2000/svg" width="100" height="40">' +
        '<rect x="0" y="0" width="100%" height="100%" fill="' + color + '" stroke-width="5" stroke="#ffffff" ></rect>' +
        '<foreignObject x="15" y="10" width="100%" height="100%">' +
        '<div xmlns="http://www.w3.org/1999/xhtml" style="font-size:20px">' +
        '<span style="color:white; text-shadow:0 0 20px #000000;">' +
        label +
        '</span>' +
        '</div>' +
        '</foreignObject>' +
        '</svg>';
      const DOMURL = window.URL || window.webkitURL || window;
      const svg = new Blob([html], { type: 'image/svg+xml;charset=utf-8' });
      return DOMURL.createObjectURL(svg);
    };
    if (data.length > 0) {
      data.map((val, idx) => {
        nodes.add({
          id: idx,
          label: `${idx + 1} 번째로 실행`,
          image: imgRenderer(template(val), val.color),
          shape: 'image',
        });
      });
      sort(data, edges);
      const network = new vis.Network(container, { nodes, edges }, option);
      network.on('click', this.handleClick);
    }
    return (
      <div id="mynetwork" style={{ height: 600 }} />
    );
  }
}

TreeView.propTypes = {
  data: PropTypes.array.isRequired,
  sort: PropTypes.func.isRequired,
  template: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  treeDirection: PropTypes.string.isRequired,
};

