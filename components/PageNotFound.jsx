import React, { PropTypes } from 'react';

const propTypes = {
  location: PropTypes.object.isRequired,
};

function PageNotFound() {
  return (
    <div className="page-not-found">
      죄송합니다. 요청하신 페이지를 찾을수가 없습니다. 😭
    </div>
  );
}

PageNotFound.propTypes = propTypes;

export default PageNotFound;
