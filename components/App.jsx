import React, { PropTypes } from 'react';
import Helmet from 'react-helmet';
import { Link } from 'react-router';
import $ from 'jquery';

import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import AppBar from 'material-ui/AppBar';
import Paper from 'material-ui/Paper';

class App extends React.Component {
  state = {
    width: window.innerWidth,
    height: window.innerHeight,
    drawer: (window.innerWidth > 1000),
  }

  componentDidMount = () => {
    $(window).resize(() => {
      this.handleResize();
    });
  }

  componentWillUnmount = () => {
    $(window).off('resize');
  }

  styles = {
    desktop: {
      paper: {
        style: {
          maxWidth: '95%',
          margin: '2.5%',
          position: 'relative',
        },
        zDepth: 3,
      },
      drawer: {
        docked: true,
        containerStyle: {
          position: 'absolute',
        },
      },
      appBar: {
        titleStyle: {
          fontWeight: 100,
        }
      },
      contents: {
        style: {
          paddingLeft: 256,
        },
        appBar: {
          showMenu: false,
        },
      },
    },
    tablet: {
      paper: {
        style: {
          minWidth: '100%',
          maxWidth: '100%',
        },
        zDepth: 0,
      },
      drawer: {
        docked: false,
        containerStyle: {},
      },
      appBar: {
        titleStyle: {
          fontWeight: 100,
        }
      },
      contents: {
        style: {},
        appBar: {
          showMenu: true,
        },
      },
    },
  }

  handleResize = () => this.setState({
    width: window.innerWidth, height: window.innerHeight, drawer: (window.innerWidth > 1000),
  })

  render() {
    const { children, routes } = this.props;
    const style = (this.state.width < 1000) ? this.styles.tablet : this.styles.desktop;
    return (
      <Paper style={style.paper.style} zDepth={style.paper.zDepth}>
        <Helmet title={`AlgoKook - ${routes[1].mapMenuTitle}`} />
        <Drawer
          open={this.state.drawer}
          containerStyle={style.drawer.containerStyle}
          docked={style.drawer.docked}
          showMenuIconButton={false}
          onRequestChange={(drawer) => this.setState({ drawer })}
          zDepth={1}
        >
          <AppBar
            title="AlgoKook 👊"
            showMenuIconButton={false}
            titleStyle={style.appBar.titleStyle}
          />
          <Link to={`${process.env.REPO_NAME}TowerOfHanoi`}><MenuItem>Tower Of Hanoi</MenuItem></Link>
          <Link to={`${process.env.REPO_NAME}FloodFill`}><MenuItem>Flood Fill</MenuItem></Link>
          <Link to={`${process.env.REPO_NAME}Sudoku`}><MenuItem>Sudoku</MenuItem></Link>

        </Drawer>
        <div style={style.contents.style}>
          <AppBar
            title={(routes.length > 0) ? routes[1].mapMenuTitle : ''}
            showMenuIconButton={style.contents.appBar.showMenu}
            titleStyle={style.appBar.titleStyle}
            onLeftIconButtonTouchTap={() => this.setState({ drawer: !this.state.drawer })}
          />
          <div>
            {React.cloneElement(children, { width: this.state.width, height: this.state.height })}
          </div>

        </div>
      </Paper>
    );
  }
}

App.propTypes = {
  children: PropTypes.element.isRequired,
  routes: PropTypes.array.isRequired,
};

export default App;
