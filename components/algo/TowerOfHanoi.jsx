import React, { PropTypes } from 'react';
import { Layer, Rect, Stage } from 'react-konva';
import RaisedButton from 'material-ui/RaisedButton';
import Chip from 'material-ui/Chip';
import Slider from 'material-ui/Slider';
import Divider from 'material-ui/Divider';
import Collapse from 'react-collapse';
import AppBar from 'material-ui/AppBar';
import Paper from 'material-ui/Paper';
import { Tabs, Tab } from 'material-ui/Tabs';
import TextField from 'material-ui/TextField';
import randomColor from 'randomcolor';
import Promise from 'bluebird';
import SourceView from '../common/SourceView';
import TreeView from '../common/TreeView';

const code = `void Hanoi(int n, int a, int b, int c) {
  if (n>0) {
    Hanoi (n-1, a, c, b);
    move(a, c);
    Hanoi (n-1, b, a, c);
  }
}`;

export default class TowerOfHanoi extends React.Component {
  state = {
    status: 0,
    amount: 4,
    chip: [],
    width: {
      window: this.props.width,
      stage: this.props.width * 0.8,
      post1: this.props.width * 0.8 * 0.25,
      post2: this.props.width * 0.8 * 0.5,
      post3: this.props.width * 0.8 * 0.75,
    },
    height: {
      window: this.props.height,
      stage: this.props.height * 0.5,
      post: this.props.height * 0.5,
    },
    post: [],
    postCoor: [],
    sourceCode: true,
  }

  componentDidMount = () => {
    this.init();
  }

  drawPost = (idx, cb) => {
    const { postCoor } = this.state;
    const post = [];
    for (let j = 1; j < postCoor[idx].length; j++) {
      for (let k = 0; k < postCoor[idx][j].length; k++) {
        post.push(
          <Rect
            x={postCoor[idx][j][k].x} y={postCoor[idx][j][k].y} width={postCoor[idx][j][k].width} height={postCoor[idx][j][k].height}
            fill={postCoor[idx][j][k].color}
            key={`rect_${idx}_${j}_${k}`}
            shadowBlur={3}
          />
        );
      }
    }
    this.setState({ idx, post }, cb);
  };

  init = () => {
    const { amount, width, height } = this.state;
    const initPostCoor = (cb) => {
      const postCoor = [
        [
          [], [], [], [],
        ],
      ];
      for (let i = amount; i > 0; i--) {
        postCoor[0][1].push({
          x: (width.post1) - (((width.stage * 0.25) / amount) * (i / 2)),
          y: height.post - (10 * ((amount - i) + 1)) - 5,
          width: ((width.stage * 0.25) / amount) * i,
          height: 10,
          color: randomColor({
            luminosity: 'light',
            format: 'rgb',
          }),
        });
      }
      this.setState({
        postCoor,
        chip: [],
        idx: 0,
      }, cb.bind(this, 0));
    };
    initPostCoor(this.drawPost);
  }

  handleChange = (e) => {
    this.setState({ [`${e.target.name}`]: e.target.value }, () => this.init());
  }

  render() {
    const { status, chip, amount, width, height, post, postCoor } = this.state;
    let { idx } = this.state;

    const hanoi = (n, a, b, c) => new Promise((resolve) => {
      if (n > 0) {
        hanoi(n - 1, a, c, b);
        postCoor[idx + 1] = JSON.parse(JSON.stringify(postCoor[idx]));
        postCoor[idx + 1][c].push(postCoor[idx + 1][a].pop());
        postCoor[idx + 1][c][postCoor[idx][c].length] = {
          x: width[`post${c}`] - (postCoor[idx + 1][c][postCoor[idx][c].length].width / 2),
          y: height.post - (10 * ((postCoor[idx + 1][c].length))) - 5,
          width: postCoor[idx + 1][c][postCoor[idx][c].length].width,
          height: postCoor[idx + 1][c][postCoor[idx][c].length].height,
          color: postCoor[idx + 1][c][postCoor[idx][c].length].color,
        };
        chip.push({
          level: n,
          from: a,
          to: c,
          color: postCoor[idx + 1][c][postCoor[idx][c].length].color,
        });
        idx += 1;
        resolve();
        hanoi(n - 1, b, a, c);
      }
    });

    const stopInterval = () => { clearInterval(this.state.timer); this.setState({ timer: null }) };

    const startInterval = () => (this.state.timer) ? false : this.setState({
      timer: setInterval(() => {
        if (this.state.idx >= this.state.chip.length) {
          stopInterval();
        } else {
          this.setState({ idx: (this.state.idx < this.state.chip.length) ? this.state.idx + 1 : 0 },
            () => this.drawPost(this.state.idx)
          );
        }
      }, 1000),
    });

    const callHanoi = () => {
      this.init();
      this.setState({ status: 1 });
      hanoi(amount, 1, 2, 3)
        .then(() => {
          this.setState({ chip, postCoor, status: 2 });
          startInterval();
        });
    };

    const treeViewSort = (data, edges) => {
      const _func = (mid) => {
        let idx = mid - 1;
        while (idx >= 0) {
          if (data[idx].level === data[mid].level - 1) {
            edges.add({
              from: mid,
              to: idx,
            });
            _func(idx);
            break;
          }
          idx--;
        }
        idx = mid + 1;
        while (idx < data.length) {
          if (data[idx].level === data[mid].level - 1) {
            edges.add({
              from: mid,
              to: idx,
            });
            _func(idx);
            break;
          }
          idx++;
        }
      };
      _func(Math.floor(data.length / 2));
    };

    const treeViewTemplate = (val) => `${val.from} → ${val.to}`;

    return (
      <div className="algo-container">
        <div className={`algo-overlay ${status == 1 ? 'show' : 'hide'}`} />
        <div className="algooption">
          <TextField
            floatingLabelText="원판 갯수"
            name="amount"
            value={amount}
            onChange={this.handleChange}
            disabled={true}
            style={{ width: '80%', marginRight: '10%' }}
          />
          <RaisedButton className="button-run" label="실행" primary={true} onClick={callHanoi} />
        </div>

        <h3>
          {idx} / {chip.length}
        </h3>
        <Stage className={`canvas-wrapper ${(amount > 1) ? '' : 'hide'}`} width={width.stage} height={height.stage}>
          <Layer>
            <Rect
              x={width.post1} y={0} width={5} height={height.post}
              fill={"#7f7f7f"}
              shadowBlur={3}
            />
            <Rect
              x={width.post2} y={0} width={5} height={height.post}
              fill={"#7f7f7f"}
              shadowBlur={3}
            />
            <Rect
              x={width.post3} y={0} width={5} height={height.post}
              fill={"#7f7f7f"}
              shadowBlur={3}
            />
            <Rect
              x={0} y={height.post - 5} width={width.stage} height={5}
              fill={"#7f7f7f"}
              shadowBlur={3}
            />
            { post }
          </Layer>
        </Stage>
        <Divider />
        <div className={(status === 2) ? 'show' : 'hide'}>
          <Slider
            defaultValue={0}
            step={1}
            min={0}
            value={this.state.idx}
            max={this.state.chip.length}
            onChange={(e, v) => { stopInterval(); this.drawPost(v); }}
          />
          <RaisedButton label="자동재생 시작" primary={true} onClick={startInterval} />
          <RaisedButton label="자동재생 중지" secondary={true} onClick={stopInterval} />
        </div>

        <Paper zDepth={1} className={`_paper ${(this.state.chip.length > 0) ? 'show' : 'hide'}`}>
          <Tabs>
            <Tab label="Tree" >
              <TreeView data={this.state.chip}
                midValue={this.state.amount}
                subtreeMaker={'towerOfHanoi'}
                template={treeViewTemplate}
                sort={treeViewSort}
                treeDirection={'UD'}
                handleClick={(idx) => { stopInterval(); this.drawPost(idx); }}
              />
            </Tab>
            <Tab label="List" >
              {this.state.chip.map((val, idx) =>
                <Chip
                  className="chip"
                  style={{ backgroundColor: val.color }}
                  key={`chip_${idx}`}
                  onClick={() => { stopInterval(); this.drawPost(idx); }}
                >
                  {`👉 ${idx} | Move disk from ${val.from} to ${val.to}`}
                </Chip>
              )}
            </Tab>
          </Tabs>
        </Paper>

        <Paper zDepth={1} className="_paper">
          <AppBar
            title="Source Code"
            showMenuIconButton={false}
            zDepth={0}
            className="app-bar"
            onClick={() => this.setState({ sourceCode: !this.state.sourceCode })}
          />
          <Collapse isOpened={this.state.sourceCode}>
            <SourceView>
              {code}
            </SourceView>
          </Collapse>
        </Paper>
        
      </div>
    );
  }
}

TowerOfHanoi.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
};
