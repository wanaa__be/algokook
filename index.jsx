import React from 'react';
import { render } from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import 'current-input';

import App from './components/App';
import TowerOfHanoi from './components/algo/TowerOfHanoi';
import FloodFill from './components/algo/FloodFill';
import Sudoku from './components/teamProject/sudoku/main';
import PageNotFound from './components/PageNotFound';

injectTapEventPlugin();
require('normalize.css');
require('./styles/app.scss');

const routes = (
  <Route path={`${process.env.REPO_NAME}`} mapMenuTitle="Algo" component={App}>
    <IndexRoute mapMenuTitle="TowerOfHanoi" component={TowerOfHanoi} />
    <Route path="TowerOfHanoi" mapMenuTitle="TowerOfHanoi" component={TowerOfHanoi} />
    <Route path="FloodFill" mapMenuTitle="FloodFill" component={FloodFill} />
    <Route path="Sudoku" mapMenuTitle="Sudoku" component={Sudoku} />

    <Route path="*" mapMenuTitle="Page Not Found" component={PageNotFound} />
  </Route>
);

const Main = () => (
  <Router
    history={browserHistory}
    routes={routes}
  />
);

render(
  <MuiThemeProvider>
    <Main />
  </MuiThemeProvider>
  ,
  document.getElementById('root')
);
