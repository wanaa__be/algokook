import React, { PropTypes } from 'react';
import { Layer, Rect, Stage } from 'react-konva';
import RaisedButton from 'material-ui/RaisedButton';
import $ from 'jquery';
import Chip from 'material-ui/Chip';
import Slider from 'material-ui/Slider';
import Divider from 'material-ui/Divider';
import Collapse from 'react-collapse';
import AppBar from 'material-ui/AppBar';
import Paper from 'material-ui/Paper';
import { Tabs, Tab } from 'material-ui/Tabs';
import TextField from 'material-ui/TextField';
import CircularProgress from 'material-ui/CircularProgress';
import randomColor from 'randomcolor';
import Promise from 'bluebird';

import SourceView from '../common/SourceView';
import TreeView from '../common/TreeView';

const code = `#define SIZE 9
void floodFill(char fig[][SIZE+1], int x, int y, char oldColor, char newColor)
{
  if (fig[x][y] != oldColor)
    return;
  else {
    fig[x][y] = newColor;
    floodFill(fig, x-1, y, oldColor, newColor);
    floodFill(fig, x+1, y, oldColor, newColor);
    floodFill(fig, x, y-1, oldColor, newColor);
    floodFill(fig, x, y+1, oldColor, newColor);
  }
}`;

export default class FloodFill extends React.Component {
  state = {
    status: 0,
    sizeRow: 7,
    sizeCol: 7,
    startRow: 2,
    startCol: 4,
    chip: [],
    width: {
      window: this.props.width,
      stage: this.props.width * 0.8,
    },
    height: {
      window: this.props.height,
      stage: this.props.height * 0.5,
    },
    boxSize: 300,
    rect: [[], []],
    rectProgress: [[[], []]],
    rectMap: [],
    color: {
      dark: randomColor({
        luminosity: 'dark',
        format: 'rgb',
      }),
      light: randomColor({
        luminosity: 'light',
        format: 'rgb',
      }),
      bright: randomColor({
        luminosity: 'bright',
        format: 'rgb',
      }),
    },
    sourceCode: true,
  }

  componentDidMount = () => {
    this.init();
  }

  drawRect = (idx, cb) => {
    const { boxSize, color, rectProgress } = this.state;
    const rectMap = [];
    for (let i = 0; i < rectProgress[idx].length; i++) {
      for (let j = 0; j < rectProgress[idx][i].length; j++) {
        const fill = (rectProgress[idx][i][j] === 0) ? color.dark :
                      (rectProgress[idx][i][j] === 1) ? color.light : color.bright;
        rectMap.push(
          <Rect
            x={j * (boxSize / rectProgress[idx][i].length)}
            y={i * (boxSize / rectProgress[idx].length)}
            width={(boxSize / rectProgress[idx][i].length)}
            height={(boxSize / rectProgress[idx].length)}
            fill={fill}
            stroke={"#ffffff"}
            key={`rect_${j}_${i}`}
          />
        );
      }
    }
    this.setState({ idx, rectMap }, cb);
  };

  init = () => {
    const { sizeRow, sizeCol } = this.state;
    const initColoring = (cb) => {
      const rect = [];
      for (let i = 0; i < sizeRow; i++) {
        const subArr = [];
        switch (i) {
          case 0:
          case sizeRow - 1:
            for (let j = 0; j < sizeCol; j++) {
              subArr.push(0);
            }
            rect.push(subArr);
            break;
          default:
            for (let j = 0; j < sizeCol; j++) {
              if (j === 0 || j === sizeCol - 1) {
                subArr.push(0);
              } else if (i === Math.floor(sizeRow / 2) &&
                          (j < Math.floor(sizeCol / 2) - 1 || j > Math.floor(sizeCol / 2) + 1)
                        ) {
                subArr.push(0);
              } else if (j === Math.floor(sizeCol / 2) &&
                          (i < Math.floor(sizeRow / 2) - 1 || i > Math.floor(sizeRow / 2) + 1)
                        ) {
                subArr.push(0);
              } else if ((j === Math.floor(sizeCol / 2) - 1 && i === Math.floor(sizeRow / 2) - 1) ||
                          (j === Math.floor(sizeCol / 2) + 1 && i === Math.floor(sizeRow / 2) + 1)
                        ) {
                subArr.push(0);
              } else {
                subArr.push(1);
              }
            }
            rect.push(subArr);
            break;
        }
      }
      this.setState({ rect, rectProgress: [rect], chip: [] }, cb.bind(this, 0));
    };
    initColoring(this.drawRect);
  }

  handleChange = (e) => {
    this.setState({ [`${e.target.name}`]: e.target.value }, () => this.init());
  }

  render() {
    const { status, chip, sizeRow, sizeCol, startRow, startCol, width, height, boxSize,
            rect, rectMap, rectProgress } = this.state;
    let { idx } = this.state;

    const floodFill = (row, col, oc, nc, level) => new Promise((resolve) => {
      if (rect[row][col] === oc) {
        rect[row][col] = nc;
        chip.push({
          row,
          col,
          idx,
          level,
        });
        idx++;
        rectProgress.push(JSON.parse(JSON.stringify(rect)));
        resolve();
        floodFill(row - 1, col, oc, nc, level + 1);
        floodFill(row + 1, col, oc, nc, level + 1);
        floodFill(row, col - 1, oc, nc, level + 1);
        floodFill(row, col + 1, oc, nc, level + 1);
      }
    });

    const stopInterval = () => { clearInterval(this.state.timer); this.setState({ timer: null }) };

    const startInterval = () => (this.state.timer) ? false : this.setState({
      timer: setInterval(() => {
        if (this.state.idx >= this.state.chip.length) {
          stopInterval();
        } else {
          this.setState({ idx: (this.state.idx < this.state.chip.length) ? this.state.idx + 1 : 0 },
            () => this.drawRect(this.state.idx)
          );
        }
      }, 1000),
    });

    const callFloodFill = () => {
      this.init();
      this.setState({ status: 1 });
      floodFill(startRow, startCol, 1, 2, 0)
        .then(() => {
          this.setState({ chip, rect, rectProgress, status: 2 });
          // startInterval();
        });
    };

    const treeViewSort = (data, edges) => {
      const _func = (last) => {
        let idx = last - 1;
        while (idx >= 0) {
          if (data[idx].level === data[last].level - 1) {
            if (edges.get(`${last}-${idx}`) !== null) {
              break;
            }
            edges.add({
              id: `${last}-${idx}`,
              from: last,
              to: idx,
            });
            _func(idx);
            _func(idx - 1);
            _func(idx - 2);
            _func(idx - 3);
            break;
          }
          idx--;
        }
      };
      _func(data.length - 1);
    };

    const treeViewTemplate = (val) => `(${val.col + 1}, ${val.row + 1})`;

    return (
      <div className="algo-container">
        <div className={`algo-overlay ${status == 1 ? 'show' : 'hide'}`} >
          <div className='algo-overlay-contents'>
            <CircularProgress size={2} />
            <div>Calculating</div>
          </div>
        </div>
        <div className="algo-option">
          <TextField
            floatingLabelText="Row Size"
            name="sizeRow"
            value={sizeRow}
            onChange={this.handleChange}
            disabled={true}
            style={{ width: '80%', marginRight: '10%' }}
          />
          <TextField
            floatingLabelText="Col Size"
            name="sizeCol"
            value={sizeCol}
            onChange={this.handleChange}
            disabled={true}
            style={{ width: '80%', marginRight: '10%' }}
          />
          <RaisedButton className="button-run" label="실행" primary={true} onClick={callFloodFill} />
        </div>

        <h3>
          {idx} / {chip.length}
        </h3>
        <Stage className={`canvas-wrapper ${(sizeRow > 1 && sizeCol > 1) ? '' : 'hide'}`} width={width.stage} height={height.stage}>
          <Layer>
            <Rect
              width={boxSize} height={boxSize}
              stroke={"#7f7f7f"}
              shadowBlur={3}
            />
            { rectMap }
          </Layer>
        </Stage>
        <Divider />
        <div className={(status === 2) ? 'show' : 'hide'}>
          <Slider
            defaultValue={0}
            step={1}
            min={0}
            value={this.state.idx}
            max={this.state.chip.length}
            onChange={(e, v) => { stopInterval(); this.drawRect(v); }}
          />
          <RaisedButton label="자동재생 시작" primary={true} onClick={startInterval} />
          <RaisedButton label="자동재생 중지" secondary={true} onClick={stopInterval} />
          <span className="small-alert-text"> * 페이지가 느려지거나 멈출 수 있으니 신중하게 눌러주세요. </span>
        </div>

        <Paper zDepth={1} className={`_paper ${(this.state.chip.length > 0) ? 'show' : 'hide'}`}>
          <Tabs>
            <Tab label="Tree" >
              <TreeView
                data={this.state.chip}
                midValue={this.state.amount}
                subtreeMaker={'floodFill'}
                template={treeViewTemplate}
                sort={treeViewSort}
                treeDirection={'DU'}
                handleClick={(idx) => { stopInterval(); this.drawRect(idx); }}
              />
            </Tab>
            <Tab label="List" >
              {this.state.chip.map((val, idx) =>
                <Chip
                  className="chip"
                  key={`chip_${idx}`}
                  onClick={() => { stopInterval(); this.drawRect(idx); }}
                >
                  {`👉 ${idx} | Coloring (${val.col + 1}, ${val.row + 1})`}
                </Chip>
              )}
            </Tab>
          </Tabs>
        </Paper>

        <Paper zDepth={1} className="_paper">
          <AppBar
            title="Source Code"
            showMenuIconButton={false}
            zDepth={0}
            className="app-bar"
            onClick={() => this.setState({ sourceCode: !this.state.sourceCode })}
          />
          <Collapse isOpened={this.state.sourceCode}>
            <SourceView>
              {code}
            </SourceView>
          </Collapse>
        </Paper>
      </div>
    );
  }
}

FloodFill.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
};
